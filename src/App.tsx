import React, { useCallback, useEffect, useState } from "react";
import "./App.css";
import SingleCard from "./components/SingleCard";

interface ICards {
  number: string;
  matched: boolean;
  id?: number;
}

const cardsNumber: ICards[] = [
  {
    number: "1",
    matched: false,
  },
  {
    number: "2",
    matched: false,
  },
  {
    number: "3",
    matched: false,
  },
  {
    number: "4",
    matched: false,
  },
  {
    number: "5",
    matched: false,
  },
  {
    number: "6",
    matched: false,
  },
  {
    number: "7",
    matched: false,
  },
  {
    number: "8",
    matched: false,
  },
];

function App() {
  const [cards, setCards] = useState<ICards[]>([]);
  const [score, setScore] = useState(0);
  const [firstChoice, setFirstChioce] = useState<ICards | null>(null);
  const [secondChoice, setSecondChioce] = useState<ICards | null>(null);

  // shuffle card
  const shuffleCard = useCallback(() => {
    const shuffleCards = [...cardsNumber, ...cardsNumber]
      .sort(() => Math.random() - 0.5)
      .map((card) => ({ ...card, id: Math.random() }));
    setCards(shuffleCards);
    setScore(0);
  }, []);

  const handleChoice = (card: ICards) => {
    if (!secondChoice) {
      firstChoice ? setSecondChioce(card) : setFirstChioce(card);
    }
  };

  useEffect(() => {
    if (firstChoice && secondChoice) {
      if (firstChoice.number === secondChoice.number) {
        setCards((prevCard: any) => {
          return prevCard.map((card: any) => {
            if (card.number === firstChoice.number) {
              return { ...card, matched: true };
            } else {
              return card;
            }
          });
        });
        setScore((prevVal) => prevVal + 10);
        resetTurn();
      } else {
        setTimeout(() => {
          setScore((prevVal) => prevVal - 5);
          resetTurn();
        }, 1000);
      }
    }
  }, [firstChoice, secondChoice]);

  useEffect(() => {
    shuffleCard();
  }, []);

  const resetTurn = () => {
    setFirstChioce(null);
    setSecondChioce(null);
  };
  return (
    <>
      <div className="main-container">
        <div className="title">Memory Game</div>
        <button className="button" onClick={shuffleCard}>
          New Game
        </button>
        <div className="score-container">
          <div className="score">Your Score is : {score}</div>
        </div>

        <div className="card-grid-container">
          <div className="card-grid">
            {cards.map((card) => (
              <SingleCard
                key={card.id}
                card={card}
                handleChoice={(card) => handleChoice(card)}
                flipped={
                  card === firstChoice || card === secondChoice || card.matched
                }
              />
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
