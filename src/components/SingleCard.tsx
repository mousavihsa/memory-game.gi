import React, { FC } from "react";
import cover from "../assets/img/coverImg.png";

interface ICards {
  number: string;
  matched: boolean;
  id?: number;
}

interface IProps {
  card: ICards;
  handleChoice: (item: ICards) => void;
  flipped: boolean;
}

const SingleCard: FC<IProps> = ({ card, handleChoice, flipped }) => {
  const handleClick = () => {
    handleChoice(card);
  };

  return (
    <div className="card">
      <div className={flipped ? "flipped" : ""}>
        <div className={"card-number-container"}>
          <div className="card-number">{card.number}</div>
        </div>
        <div className="card-img-container" onClick={handleClick}>
          <img className="card-cover" src={cover} alt="cover img" />
        </div>
      </div>
    </div>
  );
};

export default SingleCard;
